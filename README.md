# Back-end
[Working example](http://backend.edurio.mywebapp.lv)

[Repo](https://jrullis@bitbucket.org/janisrullis/edurio-backend.git)

## How to setup?

```
git clone https://jrullis@bitbucket.org/janisrullis/edurio-backend.git
sudo chown www-data:www-data edurio-backend -R
sudo chmod a+rwX edurio-backend -R
cd edurio-backend
cp .env.example .env
composer install
```
Set your database config.
Set API_DOMAIN=to the current domain in the config/app.php.
```
php artisan key:generate
php artisan migrate
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\JWTAuthServiceProvider"
php artisan jwt:generate
```

## Where to look?
See resources/assets/js/components there happens the main magic.

## Front-end
[Working example](http://edurio.mywebapp.lv)

[Repo](https://jrullis@bitbucket.org/janisrullis/edurio-frontend.git)

## License
MIT License. See LICENSE file.

## Credits
Thanks to Koen Calliauw @kcalliauw for https://github.com/layer7be/vue-starter-laravel-api.
This repo is mostly based on it.