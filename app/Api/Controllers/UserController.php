<?php

namespace Api\Controllers;

use Hash;
use App\User;
use Api\Transformers\UserTransformer;
use App\Http\Requests;
use Illuminate\Http\Request;
use Api\Requests\UserRequest;

class UserController extends BaseController {

    public function getPagedUsers() {

        $users = User::paginate(2);

        return $this->response->paginator($users, new UserTransformer);
    }

    public function getUsers() {

        $users = User::all();

        return $this->response->collection($users, new UserTransformer);
    }

    /**
     * Store a new user in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request) {
        
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        
        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return $this->item(User::findOrFail($id), new UserTransformer);
    }

    /**
     * Update the user in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id) {
        $user = User::findOrFail($id);
        $user->update($request->only(['name', 'email']));
        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        return User::destroy($id);
    }

}
