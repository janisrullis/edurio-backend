<?php

namespace Api\Controllers;

use App\Dog;
use App\Http\Requests;
use Illuminate\Http\Request;
use Api\Requests\DogRequest;
use Api\Transformers\DogTransformer;
use Log;
use Excel;

/**
 * @Resource('Dogs', uri='/dogs')
 */
class DogsController extends BaseController {

    public function __construct() {
//        $this->middleware('jwt.auth');
    }

    /**
     * Show all dogs
     *
     * Get a JSON representation of all the dogs
     * 
     * @Get('/')
     */
    public function index() {
        return $this->collection(Dog::all(), new DogTransformer);
    }

    /**
     * Store a new dog in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DogRequest $request) {
        return Dog::create($request->only(['name', 'code', 'color', 'age']));
    }

    public function import(DogRequest $request) {

        $return = false;

        $filePath = $_FILES['file']['tmp_name'];
        $data = Excel::load($filePath, function($reader) {
                    
                })->get();

        // Has data in the uploaded Excel file
        if (!empty($data) && $data->count()) {

            foreach ($data as $key => $value) {

                // Parse name - remove spaces and tags
                $parsedName = trim(strip_tags($value->name));

                // Require not empty name
                if (strlen($parsedName)) {

                    $dog = new Dog;

                    // Set values
                    $fields = ['name', 'code', 'color', 'age'];

                    foreach ($fields as $field) {

                        if (!empty($value->$field)) {

                            $dog->$field = $value->$field;
                        }
                    }

                    $dog->save();

                    $return = true;
                }
            }
        }

        return $return ? 'success' : 'error';

//  Do a simple validation

        move_uploaded_file($_FILES['file']['tmp_name'], '/var/www/edurio/excel.ods');

        exit;
        return Dog::create($request->only(['name', 'code', 'color', 'age']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return $this->item(Dog::findOrFail($id), new DogTransformer);
    }

    /**
     * Update the dog in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DogRequest $request, $id) {
        $dog = Dog::findOrFail($id);
        $dog->update($request->only(['name', 'code', 'color', 'age']));
        return $dog;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        return Dog::destroy($id);
    }

}
