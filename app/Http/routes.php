<?php

Route::get('/', 'HomeController@index');

// Dingo API routes
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', [], function ($api) {

    // Set common namespace to Api\Controllers
    $api->group(['namespace' => 'Api\Controllers', 'middleware' => '\Barryvdh\Cors\HandleCors::class'], function ($api) {

        // Routes without authorization
        $api->get('unauth', function () {

            return 'This is a page without authorization';
        });
        $api->get('unauth_json', function () {

            return ['message'=>'This is a page without authorization'];
        });

        $api->post('login', 'AuthController@authenticate');
        $api->post('register', 'AuthController@register');

        // Routes that require authorization
        $api->group(['middleware' => 'jwt.auth'], function ($api) {

            $api->get('validate_token', 'AuthController@validateToken');

            // Users
            $api->get('users', 'UserController@getUsers');
            $api->get('users/me', 'AuthController@me');
            $api->post('users', 'UserController@store');
            $api->get('users/{id}', 'UserController@show');
            $api->delete('users/{id}', 'UserController@destroy');
            $api->put('users/{id}', 'UserController@update');

            // Dogs
            $api->get('dogs', 'DogsController@index');
            $api->post('dogs', 'DogsController@store');
            $api->post('dogs/import', 'DogsController@import');
            $api->get('dogs/{id}', 'DogsController@show');
            $api->delete('dogs/{id}', 'DogsController@destroy');
            $api->put('dogs/{id}', 'DogsController@update');
        });
    });
});
