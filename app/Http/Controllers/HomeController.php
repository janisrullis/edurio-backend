<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller {

    public function index() {

        return 'Backend based on Laravel 5.2 with Dingo API and JWT';
    }

}
