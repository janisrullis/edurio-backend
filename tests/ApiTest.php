<?php

/* *
 * Can be useful for debugging:
 * dd($this->json('POST', '/register', $data)->response->getContent());
 * 
 * TODO:
 * * CRUD users
 * * Insert test users that would be available in all tests
 */

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApiTest extends TestCase {

    use DatabaseTransactions;

    public function testUnauth() {

        $this->visit('/')->see('Backend based on Laravel 5.2 with Dingo API and JWT');
        $this->visit('unauth')->see('This is a page without authorization');
        $this->json('GET', 'unauth_json', [])->seeJson(['message' => 'This is a page without authorization']);
    }

    public function testRegistration() {

        $data = [
            'name' => 'name',
            'email' => 'test@example.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];

        $token = $this->returnsToken('/register', $data, 'POST');

        // Instantly after registration can access the profile info
        $this->isValidUser('me', $token);

        // Can log in
        $token = $this->login($data['email'], $data['password']);
    }

    public function testLogin($email = 'Incorrect@login.com', $password = 'password') {

        // Missing values
        $response = $this->json('POST', '/login', [])
                ->seeJson(['error' => 'invalid_credentials']);

        // Incorrect login
        $response = $this->json('POST', '/login', ['email' => $email, 'password' => $password])
                ->seeJson(['error' => 'invalid_credentials']);

        // Correct login
        $this->login();
    }

    public function testUsersList() {

        // Can not access because do not have a token
        $this->tokenMissing('/users', []);

        // Login and get a token
        $token = $this->login();

        // In the list must bet at least one user (current user)
        $response = $this->jsonWithToken($token, 'GET', '/users', []);
        $list = $this->getResponseContent($response);
        $this->assertNotEmpty($list->data);
    }

    protected function getResponseContent($response, $assoc = false) {

        return json_decode($response->response->content(), $assoc);
    }

    protected function login($email = 'existing@example.com', $password = 'password') {

        $token = $this->returnsToken('/login', ['email' => $email, 'password' => $password], 'POST');
        $this->isValidUser('me', $token);

        return $token;
    }

    protected function isValidUser($userId, $token, array $data = [], array $headers = []) {

        // Pass the token and ask for user's data
        $response = $this->jsonWithToken($token, 'GET', '/users/' . $userId, [])->seeJsonStructure([ 'user' => ['email']]);

        // Check if a correct user has been returned
        $user = $this->getResponseContent($response, true)['user'];

        $this->isValidUserData($user, $data);
    }

    protected function isValidUserData($user, $data) {

        // Compare each field
        foreach ($data as $key => $val) {

            $this->assertEquals($data[$key], $user[$key]);
        }

        $this->assertGreaterThan(0, $user['id'], 'ID must be greater than zero. Maybe autoincrement has failed');
    }

    protected function jsonWithToken($token, $method, $uri, array $data = [], array $headers = []) {

        // Add token in the header
        $headers['Authorization'] = 'Bearer: ' . $token;

        return $this->json($method, $uri, $data, $headers);
    }

    /**
     * Shorthand for testing if a token is returned
     * @param type $url
     * @param type $method
     * @param type $data
     */
    protected function returnsToken($url, $data = [], $method = 'GET') {

        $response = $this->json($method, $url, $data)->seeJsonStructure([ 'token']);

        // Validate token, if it was received
        $token = $this->getResponseContent($response)->token;
        $this->assertTrue(strlen($token) > 15, 'Token is too short');

        return $token;
    }

    protected function tokenMissing($url, $data = [], $method = 'GET') {

        $this->json($method, $url, $data)->seeJson([ 'error' => 'token_not_provided']);
    }

}
